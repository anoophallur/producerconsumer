LIBS = -lpthread
CC = gcc

default: async_cat sync_cat

async_cat.o: async_cat.c
	$(CC) -c async_cat.c -o async_cat.o

async_cat: async_cat.o
	$(CC) async_cat.o $(LIBS) -o async_cat

sync_cat.o: sync_cat.c
	$(CC) -c sync_cat.c -o sync_cat.o

sync_cat: sync_cat.o
	$(CC) sync_cat.o $(LIBS) -o sync_cat


clean:
	-rm -f *.o
	-rm -f async_cat
	-rm -f sync_cat
