#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFFER_SIZE 1024

int main(int argc, char* argv[]){

    /* Size of char is 1 byte.     printf("%lu", sizeof(char)); is 1*/
    /* Three 1k buffers, alloted from the stack */
    char buffer[BUFFER_SIZE];
    char* filename;
    ssize_t ret_in,ret_out;

    if(argc != 2){
        perror("Usage: ./sync_cat `filename`\n");
        return EXIT_FAILURE;
    }

    filename = argv[1];
    int fd = open(filename, O_RDONLY);

    if(fd == -1){
        perror("open");
        return EXIT_FAILURE;
    } else {
        while((ret_in = read (fd, &buffer, BUFFER_SIZE)) > 0){
            ret_out = write (STDOUT_FILENO, &buffer, ret_in);
            if(ret_out != ret_in){
                /* Write error */
                perror("write");
                return EXIT_FAILURE;
            }
        }
    }
    close(fd);
    return EXIT_SUCCESS;
}
