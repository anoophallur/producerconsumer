#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

/* Size of char is 1 byte*/
/* Three 1k buffers, alloted from the stack */
#define BUFFER_SIZE 1024
#define NUM_BUFFERS 3
char buffer[NUM_BUFFERS][BUFFER_SIZE];

/* Initializing variables for mutex locking */
pthread_mutex_t mutex;
int read_counter = 0;
int write_counter = 0;
int size=0;
ssize_t ret_out, ret_in;

/* Thread for reading into the file and writing to buffer */
void *read_thread(void *arg);
/* Thread for reading from the buffer and writing to STDOUT */
void *write_thread(void *arg);
/********************************************/

int main(int argc, char *argv[]) {
    pthread_t tid[2];
    if (argc != 2) {
        perror("Usage: ./async_cat `filename`\n");
        return EXIT_FAILURE;
    }

    if (pthread_mutex_init(&mutex, NULL) != 0) {
        perror("Mutex initialization failed\n");
        return 1;
    }

    int err1 = pthread_create(&(tid[0]), NULL, read_thread, argv[1]);
    int err2 = pthread_create(&(tid[1]), NULL, write_thread, NULL);
    if (err1|| err2) {
        perror("Cant create thread\n");
        return EXIT_FAILURE;
    } else {
        /* Wait for children threads to terminate */
        err1 = pthread_join(tid[0], NULL);
        err2 = pthread_join(tid[1], NULL);
        if(err1|| err2){
            perror("Child threads killed unexpectedly\n");
        }
    }
    return EXIT_SUCCESS;
}


/* Thread for reading into the file and writing to buffer */
void *read_thread(void *arg) {
    char *filename = (char *) arg;
    int fd = open(filename, O_RDONLY);

    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    } else {
        do {
            pthread_mutex_lock(&mutex);
            /* Critical section begin */
            if(size < NUM_BUFFERS){
                ret_in = read(fd, &buffer[read_counter++ % NUM_BUFFERS],
                    BUFFER_SIZE);
                size++;
            }
            /* Critical section end */
            pthread_mutex_unlock(&mutex);
        } while (ret_in > 0);
        write_counter = -1;/*setting to -1 to indicate end of write */
    }
    close(fd);
    return NULL;
}

/* Thread for reading from the buffer and writing to STDOUT */
void *write_thread(void *arg) {
    while(write_counter != -1){
        if(size > 0){
            pthread_mutex_lock(&mutex);
            /* Critical section begin */
            ret_out = write(STDOUT_FILENO, &buffer[write_counter++ % NUM_BUFFERS],
                    ret_in);
            size--;
            /* Critical section end */
            pthread_mutex_unlock(&mutex);
            if (ret_out < 0) {
                /* Write error */
                perror("write");
                exit(EXIT_FAILURE);
            }

        }
    }
    return NULL;
}
