In this problem, I try to simulate the `cat` tool found in glibc, but with two
separate threads,one for writing to stdout and another for reading from the
file.

I'm also including the synchrounous version of the same program, where the
read and write are done in a single thread, just for comparision and correctness

To compile,

> make

Generates an executable async_cat and sync_cat

To execute it,

> ./ async_cat <filename>

or

> ./ sync_cat <filename>

example

> ./ async_cat README.md
